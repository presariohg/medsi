function out=simplevisualcrypt(in, sessionkey)
% simplevisualcrypt.m
% Author: Frank Hartung
% May 2017

% Calculate size of input picture
[dimy, dimx] = size(in);

% Allocate output picture of same type and size as input
out=in*0;

% Re-cast session key
sessionkey = double(sessionkey);

% Calculate random array, using session kex as seed
% this is a so-called one-time pad
rng(sessionkey);
pad = double(round(255*rand(dimy,dimx)));

% Encrypt input by XOR-ing with random array
% XOR-ing a second time with the same random array will restore the original pixels 
for i=1:dimy
    for j=1:dimx
        out(i,j)=bitxor(in(i,j),pad(i,j));        
    end
end