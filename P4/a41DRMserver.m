% a41DRMserver.m
% Author: Frank Hartung
% May 2017

% Clear variables
clear

% Read in picture from file system, re-cast and determine picture size
picture=imread('lena512.bmp');
picture=uint8(picture);
[dimy, dimx] = size(picture);

% Display original picture
figure
image(picture);
colormap(gray(256));
daspect([1 1 1])
title('Original picture');
drawnow

% Generate session key and print to stdout
session_key = double(randi(255));

% Encrypt the picture with session key
picture_protected = simplevisualcrypt(picture, session_key);

% Display protected picture
figure
image(picture_protected);
colormap(gray(256));
daspect([1 1 1])
title('Protected picture');
drawnow


% Save protected picture to 'content object'
save('contentobject', 'picture_protected');

% Load device public key
modulus = load('devicepublickey.mat').Modulus; % c = m^e%n, m = c^d%n
exponent = load('devicepublickey.mat').PublicExponent;

% PKI encrypt session key with device public key
session_key_encrypted = Encrypt(modulus, exponent, session_key);

% Save encrypted session key to 'rights object'
save('rightsobject', 'session_key_encrypted');

% Clear variables
clear
