% a41PKI.m
% Author: Frank Hartung
% May 2017

% Call PKI function and generate private/public kex pair
% Source: https://de.mathworks.com/matlabcentral/fileexchange/53457-rsa-public-key-encryption-and-signing--32bit-
% with modification by Frank Hartung

[Modulus, PublicExponent, PrivateExponent] = GenerateKeyPair2;

% print PKI key pair variables
fprintf('PKI Key Pair:\n')
fprintf('Modulus:                '), fprintf('%5d\n', Modulus)
fprintf('Public Exponent:        '), fprintf('%5d\n', PublicExponent)
fprintf('Private Exponent:       '), fprintf('%5d\n', PrivateExponent)

% save private and public keys in separate files
save('devicepublickey', 'Modulus', 'PublicExponent')
save('deviceprivatekey', 'Modulus', 'PrivateExponent')

% Clear variables  
clear Modulus
clear PublicExponent
clear PrivateExponent
