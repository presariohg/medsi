% a41DRMclient.m
% Author: Frank Hartung
% May 2017

% Clear variables
clear

% Load content object, rights oject, Device private key
content_object = load('contentobject.mat').picture_protected;
rights_object = load('rightsobject.mat').session_key_encrypted;
modulus = load('deviceprivatekey.mat').Modulus;
exponent = load('deviceprivatekey.mat').PrivateExponent;

% Decrypt encrypted session key
session_key = Decrypt(modulus, exponent, rights_object);

% Decrypt protected picture using session key
% MODIFY HERE
decrypted_picture = simplevisualcrypt(content_object, session_key);

% Display decrypted picture; this should be the same as original picture
figure
image(decrypted_picture);
colormap(gray(256));
daspect([1 1 1])
title('Decrypted picture');
drawnow

