% a41.m
% Author: Frank Hartung
% May 2017

% Calls the other three m-files in the right order

% Clean up - erase all variables, close all figures
clear 
close ALL

% Execute the PKI CA function and generate device public and private keys
a41PKI

% Execute the DRM server function and generate content object and rights
% object = license
a41DRMserver

% Execute the DRM client function and decrypt the protected picture
a41DRMclient