clear all
close all

img = imread("lena512.bmp");
img = int16(img); % Cast to int16

figure % A1-2a figure
image(img)
colormap(gray(256)) % 8 bit grayscale, monochrome
daspect([1 1 1])

% A1-2 patchwork
[Y_MAX, X_MAX] = size(img);
matrix_a = zeros([Y_MAX, X_MAX]);
matrix_a_wm = zeros([Y_MAX, X_MAX]);
matrix_b = zeros([Y_MAX, X_MAX]);
matrix_b_wm = zeros([Y_MAX, X_MAX]);

boolean_mask = zeros([Y_MAX, X_MAX]);

for i = 1 : (Y_MAX * X_MAX / 2)
    is_pixel_set = false;
    while (~is_pixel_set)
        random_x = randi([1, X_MAX]);
        random_y = randi([1, Y_MAX]);

        if (boolean_mask(random_x, random_y) == 0)
            boolean_mask(random_x, random_y) = 1;
            is_pixel_set = true;
        end
    end
end

 
sum_diff = int64(0);
sum_diff_wm = int64(0);

for y = 1 : Y_MAX
    for x = 1 : X_MAX
        if boolean_mask(x, y)
            matrix_a(x, y) = img(x, y);
            matrix_a_wm(x, y) = matrix_a(x, y) + 1;
        else
            matrix_b(x, y) = img(x, y);
            matrix_b_wm(x, y) = matrix_b(x, y) - 1;
        end

        sum_diff = sum_diff + matrix_a(x, y) - matrix_b(x, y);
        sum_diff_wm = sum_diff_wm + matrix_a_wm(x, y) - matrix_b_wm(x, y);
    end
end

sum_diff_wm
sum_diff
"absolute values of sum diff with watermark is much bigger than of sum diff without watermark"

sum_diff = int64(0);
sum_diff_wm = int64(0);

for y = 1 : Y_MAX
    for x = 1 : X_MAX
        sum_diff = sum_diff + matrix_a(x, y) - matrix_b(x, y);
        sum_diff_wm = sum_diff_wm + matrix_a_wm(x, y) - matrix_b_wm(x, y);
    end
end
sum_diff_wm
sum_diff

figure("Name", "Matrix A")
image(matrix_a)
colormap(gray(256)) % 8 bit grayscale, monochrome
daspect([1 1 1])

figure("Name", "Matrix B")
image(matrix_b)
colormap(gray(256)) % 8 bit grayscale, monochrome
daspect([1 1 1])


figure("Name", "Matrix A with watermark")
image(matrix_a_wm)
colormap(gray(256)) % 8 bit grayscale, monochrome
daspect([1 1 1])

figure("Name", "Matrix B with watermark")
image(matrix_b_wm)
colormap(gray(256)) % 8 bit grayscale, monochrome
daspect([1 1 1])

"you can't tell the different with the naked eyes but the sum diff values clearly show which pair is watermarked"