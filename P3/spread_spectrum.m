function blind_detector(orig_img, b, root_a)

[Y_MAX, X_MAX] = size(orig_img);
wm_key = zeros([Y_MAX, X_MAX]);
watermarked_img = zeros([Y_MAX, X_MAX]);

for y = 1 : Y_MAX
    for x = 1 : X_MAX
        coin_toss = randi([0, 1]);
        if coin_toss
            wm_key(x, y) = -1;
        else
            wm_key(x, y) = 1;
        end

        watermarked_img(x, y) = b * root_a * wm_key(x, y) + orig_img(x, y);
    end
end

writematrix(wm_key, "./wm_key.txt", "Delimiter", " ");
imwrite(watermarked_img, gray(256), "./wm_img.bmp");