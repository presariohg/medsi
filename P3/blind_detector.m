function [detected_bit, corsum] = blind_detector(watermarked_img, root_a, wm_key)

[Y_MAX, X_MAX] = size(watermarked_img);

% blind detector
correlation_sum = 0;

for y = 3 : Y_MAX - 2
    for x = 3 : X_MAX - 2
        correlation_sum = correlation_sum + watermarked_img(x, y) * wm_key(x, y);
    end
end

detected_bit = double(correlation_sum) / (root_a * X_MAX * Y_MAX);
corsum = correlation_sum;