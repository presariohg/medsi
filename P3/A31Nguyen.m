clear all
close all

img = int64(imread("lena512.bmp"));

spread_spectrum(img, 1, 1);

watermarked_img = int64(imread("wm_img.bmp"));

wm_key = readmatrix("wm_key.txt");

root_a = 1;
[Y_MAX, X_MAX] = size(watermarked_img);

figure("Name", "Watermarked") % A1-1a figure
image(watermarked_img)
colormap(gray(256)) % 8 bit grayscale, monochrome
daspect([1 1 1])



amplification_factor = 64;


"before attacking"
before_attack = blind_detector(watermarked_img, root_a, wm_key)

% %%%%%%%%%%%%%%%%%% A)
gaussian_noise = amplification_factor * int64(randn(size(watermarked_img)));
damaged_img = watermarked_img + gaussian_noise;

figure("Name", "With noise") % A1-1a figure
image(damaged_img)
colormap(gray(256)) % 8 bit grayscale, monochrome
daspect([1 1 1])

"after attacking"
[after_attack, corsum] = blind_detector(damaged_img, root_a, wm_key)

"After attacking the detected bit is changed quite a bit"
after_attack - before_attack

%%%%%%%%%%%%%%%%%% B)
% for n = 3 : 2 : 25
n = 3
pad_amount = n - 2;
padded = padarray(watermarked_img, [pad_amount pad_amount], 128, 'both');
% size(padded)

filtered = zeros(size(watermarked_img)) + 128;
blurred = zeros(size(watermarked_img)) + 128;

% low pass filter
for y = pad_amount+1 : Y_MAX+pad_amount
    for x = pad_amount+1 : X_MAX+pad_amount
        blurred(x-pad_amount, y-pad_amount) = mean(padded(x-pad_amount : x+pad_amount, y-pad_amount : y+pad_amount), 'all');

        filtered(x-pad_amount, y-pad_amount) = watermarked_img(x-pad_amount, y-pad_amount) - blurred(x-pad_amount, y-pad_amount);
    end
end

figure("Name", "Blurred") % A1-1a figure
image(blurred)
colormap(gray(256)) % 8 bit grayscale, monochrome
daspect([1 1 1])

after_attack = blind_detector(filtered, root_a, wm_key)

% end


% %%%%%%%%%%%%%%%%%% C)
shifted = watermarked_img;
shift_amount = 5;
shifted(1:X_MAX, 1:Y_MAX-shift_amount) = watermarked_img(1:X_MAX, 1+shift_amount:Y_MAX);
shifted(1:X_MAX, Y_MAX-shift_amount+1:Y_MAX) = watermarked_img(1:X_MAX, 1:shift_amount);

[after_attack, corsum] = blind_detector(shifted, root_a, wm_key)

figure("Name", "Shifted") % A1-1a figure
image(shifted)
colormap(gray(256)) % 8 bit grayscale, monochrome
daspect([1 1 1])