import os
import random

folder = 'foreground'
names = os.listdir(f"{folder}/")
id_num = 6298
count = 0
for name in names:
    count += 1
    os.rename(f"{folder}/{name}", f"{folder}/FHAC-{id_num}-pic-fg-{count:03d}.jpg")