% a52.m
% Author: Frank Hartung
% June 2017

% Clean up - erase all variables, close all figures
clear 
close ALL

% Define variables
correlations=zeros(1,16);
corrindex=1;
RSPN=single(zeros(1024,1024));

% Set file name variables for background images
filenametemplate = './imgs/background/FHAC-6298-pic-bg-';
fileindexstart = 1;
fileindexend = 50;

% Calculate RSPN from 50 background images
for i = fileindexstart:fileindexend,
    
    % Read file
    fileindex = sprintf('%03d',i);
    filename = strcat(filenametemplate, fileindex, '.jpg')
    bgimage = imread(filename);
    
    % Re-format image to gray value image and int16 type
    bgimage=int16(rgb2gray(bgimage));
   
    % Plot image
    figure(1)
    image(bgimage);
    colormap(gray(256));
    daspect([1 1 1])
    title('Own background photo');
    drawnow
     
    % Calculate SPN for that photo and accumulate to RSPN
    RSPN = RSPN + SPNextractor(bgimage);
    
end

% Divide RSPN by number of used photos
RSPN = RSPN / 50;

% Calculate autocorrelation value for normalization of later SPNs
RSPNsum=sum(sum(RSPN.*RSPN));

% Write this value to vector for later display
correlations(corrindex)=1;
corrindex=corrindex+1;

% Set file name variables for foreground images
filenametemplate = './imgs/foreground/FHAC-6298-pic-fg-';
fileindexstart = 1;
fileindexend = 10;

% Calculate SPN for 10 own foreground images
for i = fileindexstart:fileindexend,
    
    % Read file
    fileindex = sprintf('%03d',i);
    filename = strcat(filenametemplate, fileindex, '.jpg')
    fgimage = imread(filename);
    
    % Re-format image to gray value image and int16 type
    fgimage=int16(rgb2gray(fgimage));
    
    % Plot image
    figure(1)
    image(fgimage);
    colormap(gray(256));
    daspect([1 1 1])
    title('Own photo');
    drawnow
    
    % Calculate SPN for that photo 
    SPN = SPNextractor(fgimage);

    % Correlate to RSPN
    correlation = sum(sum(SPN.*RSPN));
    
    % Normalize to RSPN autocorrelation value
    correlation = correlation / RSPNsum;

    % Write this value to vector for later display
    correlations(corrindex)=correlation;
    corrindex=corrindex+1;
    
end

% Set file name variables for background images
filenametemplate = './imgs/unknown/Picture_';
fileindexstart = 1;
fileindexend = 5;

% Calculate SPN for at least 5 foreground images from other cameras
for i = fileindexstart:fileindexend,
     
    % Read file
    fileindex = sprintf('%d',i);
    filename = strcat(filenametemplate, fileindex, '.jpg')
    testimage = imread(filename);

    % Re-format image to gray value image and int16 type
    testimage=int16(rgb2gray(testimage));

    % Plot image
    figure(1)
    image(testimage);
    colormap(gray(256));
    daspect([1 1 1])
    title('Other photo');
    drawnow
    
    % Calculate SPN for that photo 
    SPN = SPNextractor(testimage);
         
    % Correlate to RSPN
    correlation = sum(sum(SPN.*RSPN))
    
    % Normalize to RSPN autocorrelation value
    correlation = correlation / RSPNsum;

    % Write this value to vector for later display
    correlations(corrindex)=correlation;
    corrindex=corrindex+1;
    
end

figure(2)
stem(correlations)
title('1: autocorrelation of RSPN, 2-11: own photos, 12-: other photos')
