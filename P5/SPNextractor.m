function SPN = SPNextractor(image)
% SPNextractor.m
% Author: Frank Hartung
% June 2017

% blow up small images
if size(image,1) < 1024
    image = imresize(image, [1024 NaN]);
end
if size(image,2) < 1024
    image = imresize(image, [NaN 1024]);
end

% selecting image center to extract a square of 1024x1024 pixels
img_center = [floor(size(image,1)/2),floor(size(image, 2)/2)];
win_size = [1024, 1024];
crop_values = [img_center(1)-floor(win_size(1)/2) img_center(2)-floor(win_size(2)/2)];

% cut out inner square of 1024x1024 pixels
square1024 = image(crop_values(1)+1:crop_values(1)+win_size(1), crop_values(2)+1:crop_values(2)+win_size(2), :);

% calculate SPN by blockwise subtraction of block mean value
SPN = single(zeros(1024,1024));
blocksize=4;

for ii=1:blocksize:1024,
    for jj=1:blocksize:1024,
        imageblock = single(image(ii:ii+blocksize-1,jj:jj+blocksize-1));
        SPNblock = imageblock - mean(mean(imageblock));
        SPN(ii:ii+blocksize-1,jj:jj+blocksize-1) = single(SPNblock);
    end
end

