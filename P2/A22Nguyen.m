clear all
close all

img = int64(imread("lena512.bmp"));

watermarked_img = int64(imread("wm_img.bmp"));

wm_key = readmatrix("wm_key.txt");

root_a = 2;

% figure("Name", "Original") % A1-1a figure
% image(img)
% colormap(gray(256)) % 8 bit grayscale, monochrome
% daspect([1 1 1])

[Y_MAX, X_MAX] = size(img);

% a) blind detector
correlation_sum = 0;

for y = 3 : Y_MAX - 2
    for x = 3 : X_MAX - 2
        correlation_sum = correlation_sum + watermarked_img(x, y) * wm_key(x, y);
    end
end

correlation_sum
blind_hidden_bit = double(correlation_sum) / (root_a * X_MAX * Y_MAX)


% b) blind detector with pre-filter
correlation_sum = 0;

for y = 3 : Y_MAX - 2
    for x = 3 : X_MAX - 2
        neighbors = img(x - 2 : x + 2, y - 2 : y + 2);
        neighbors_wm = watermarked_img(x - 2 : x + 2, y - 2 : y + 2);

        correlation_sum = correlation_sum + (watermarked_img(x, y) - mean(neighbors_wm, "all")) * wm_key(x, y);
    end
end

correlation_sum
blind_b_hidden_bit = double(correlation_sum) / (root_a * X_MAX * Y_MAX)

% c) non-blind detector
correlation_sum = 0;

for y = 3 : Y_MAX - 2
    for x = 3 : X_MAX - 2
        correlation_sum = correlation_sum + (watermarked_img(x, y) - img(x, y)) * wm_key(x, y);
    end
end

correlation_sum
non_blind_hidden_bit = double(correlation_sum) / (root_a * X_MAX * Y_MAX);
sprintf('%.6f', non_blind_hidden_bit)