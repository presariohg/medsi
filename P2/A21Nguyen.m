clear all
close all

img = imread("lena512.bmp");
img = int16(img); % Cast to int16


% figure("Name", "Original") % A1-1a figure
% image(img)
% colormap(gray(256)) % 8 bit grayscale, monochrome
% daspect([1 1 1])

b = 1;
root_a = 2;
[Y_MAX, X_MAX] = size(img);
wm_key = zeros([Y_MAX, X_MAX]);
watermarked_img = zeros([Y_MAX, X_MAX]);

for y = 1 : Y_MAX
    for x = 1 : X_MAX
        coin_toss = randi([0, 1]);
        if coin_toss
            wm_key(x, y) = -1;
        else
            wm_key(x, y) = 1;
        end

        watermarked_img(x, y) = b * root_a * wm_key(x, y) + img(x, y);
    end
end

writematrix(wm_key, "./wm_key.txt", "Delimiter", " ");
imwrite(watermarked_img, gray(256), "./wm_img.bmp");

"any root_a value >4 is considerably more noisy than the original image"

% figure("Name", "Watermarked") % A1-1a figure
% image(watermarked_img)
% colormap(gray(256)) % 8 bit grayscale, monochrome
% daspect([1 1 1])